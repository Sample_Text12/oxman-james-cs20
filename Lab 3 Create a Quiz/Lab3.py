# James Oxman

# Lab 2: Quiz
score = 0

print()

print("Welcome to my computer science quiz")

print()

print("Don't forget to make sure all of your worded answers start with capitals!")

print()

# First input variable
firstQuestion.lower = input("How much ram can a 32-bit system handle? ")

# If logic statement
if firstQuestion == "4 gigabytes" or firstQuestion == "4 gigs":
    print("Correct!")
    score += 1
else:
    print("Incorrect \nBetter luck next time!")

# Second input variable
secondQuestion = input("Name one of the cpu registers (64-bit): ")

# If logic statement
if secondQuestion == "Accumulator register" or secondQuestion == "Counter register" or secondQuestion == "Data register" or  secondQuestion == "Base register" or \
         secondQuestion == "Stack pointer register" or secondQuestion == "Stack base pointer register" or secondQuestion == "Source index register" or\
         secondQuestion == "Destination index register":
    print("Correct!")
    score += 1
else:
    print("Incorrect \nBetter luck next time!")

# Third input variable
thirdQuestion = input("From what language does the @ symbol come from? ")

# If logic statement
if thirdQuestion == "Latin":
    print("Correct!")
    score += 1
else:
    print("Incorrect \nBetter luck next time!")

# Fourth input variable
fourthQuestion = input("Calculate 64 * 64: ")

# If and elif logic statement
if fourthQuestion == "4096":
    print("Correct!")
    score += 1
else:
    print("Incorrect \nBetter luck next time!")

# Final question variables
a = 1971

b = 1981

c = 2018

d = 1999

# Final question input + Printing final question multiple choice portion
finalQuestion = input("When was the first email sent?" + "\nA)1971\nB) 1981\nC) 2018\nD) 1999 \n")

# Final question logic
if finalQuestion == "A":
    print("correct!")
    score += 1

elif finalQuestion == "C":
    print("\_(*-*)_/")

else:
    print("Incorrect \nBetter luck next time!")

# Calculating percentage of score variable
score = float(score / 5 * 100)

# Final score
print("Your final score is: ", score)

