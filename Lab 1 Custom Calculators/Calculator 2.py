# James Oxman

# Calculator 2

print("Welcome to the trapezoid area calculator")

# Input to find height
height = input("Enter the height of your trapezoid: ")

# Input to find length of bottom base
bottomBase = input("Enter length of bottom base: ")

# Input to find length of top base
topBase = input("Enter length of top base: ")

# Converting all variables into floating point values
height = float(height)

bottomBase = float(bottomBase)

topBase = float(topBase)

# Final Calculation
final = (topBase + bottomBase) * height / 2

print("This is the area of your trapezoid", final)