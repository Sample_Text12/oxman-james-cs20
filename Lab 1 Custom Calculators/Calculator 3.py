# James Oxman

# Calculator 3

import math

print()
print("This calculator calculates the volume of a right cylinder")
print()

# User's input for variables
radius = input("Enter radius of cylinder: ")
print()
height = input("Enter height of cylinder: ")

# Changing variables into floats
radius = float(radius)

height = float(height)

# Equation for radius
radius = radius * 2

# Final equation
final = math.pi * radius * height

print("Here is the volume of your right cylinder: ", final)
