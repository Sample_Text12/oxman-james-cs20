# James
# Oxman



# Lab 1 Calculators


print("Welcome to the fahrenheit to celsius calculator")

# User input
fahrenheit = input("Enter fahrenheit value: ")

# Converts user's input into a floating point value
fahrenheit = float(fahrenheit)

# Calculation
final = (fahrenheit - 32) * 5 / 9

# Final answer
print("Temperature in celsius", final)


# James Oxman

# Calculator 2

print("Welcome to the trapezoid area calculator")

# Input to find height
height = input("Enter the height of your trapezoid: ")

# Input to find length of bottom base
bottomBase = input("Enter length of bottom base: ")

# Input to find length of top base
topBase = input("Enter length of top base: ")

# Converting all variables into floating point values
height = float(height)

bottomBase = float(bottomBase)

topBase = float(topBase)

# Final Calculation
final = (topBase + bottomBase) * height / 2

print("This is the area of your trapezoid", final)


# James Oxman

# Calculator 3

import math

print()
print("This calculator calculates the volume of a right cylinder")
print()

# User's input for variables
radius = input("Enter radius of cylinder: ")
print()
height = input("Enter height of cylinder: ")

# Changing variables into floats
radius = float(radius)

height = float(height)

# Equation for radius
radius = radius * 2

# Final equation
final = math.pi * radius * height

print("Here is the volume of your right cylinder: ", final)