import pygame

# Define some colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 102, 34)
RED = (153, 0, 0)
DEEP_RED = (128, 0, 0)
ORANGE = (226, 125, 9)
PINK = (228, 9, 232)
VIOLET = (144, 16, 242)
BLUE = (13, 94, 224)
YELLOW = (255, 200, 0)
WHEAT = (218, 189, 111)
BROWN = (96, 63, 31)
BARN_ROOF = (0, 51, 153)
FRONT_BARN_ROOF = (0, 32, 126)

pygame.init()

# Set the width and height of the screen [width, height]
size = (1280, 720)
screen = pygame.display.set_mode(size)

pygame.display.set_caption("Green Fields")


# Loop until the user clicks the close button.
done = False

# Used to manage how fast the screen updates
clock = pygame.time.Clock()

# -------- Main Program Loop -----------
while not done:
    # --- Main event loop
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True

    # --- Game logic should go here
    mousePosition = pygame.mouse.get_pos()
    mouseX = mousePosition[0]
    mouseY = mousePosition[1]

    print("(" + str(mouseX) + "," + str(mouseY) + ")")

    # --- Screen-clearing code goes here

    # Here, we clear the screen to white. Don't put other drawing commands
    # above this, or they will be erased with this command.

    # If you want a background image, replace this clear with blit'ing the
    # background image.

    # https://www.pygame.org/wiki/GradientCode
    def fill_gradient(surface, color, gradient, rect=None, vertical=True, forward=True):
        if rect is None: rect = surface.get_rect()
        x1, x2 = rect.left, rect.right
        y1, y2 = rect.top, rect.bottom
        if vertical:
            h = y2 - y1
        else:
            h = x2 - x1
        if forward:
            a, b = color, gradient
        else:
            b, a = color, gradient
        rate = (
            float(b[0] - a[0]) / h,
            float(b[1] - a[1]) / h,
            float(b[2] - a[2]) / h
        )
        fn_line = pygame.draw.line
        if vertical:
            for line in range(y1, y2):
                color = (
                    min(max(a[0] + (rate[0] * (line - y1)), 0), 255),
                    min(max(a[1] + (rate[1] * (line - y1)), 0), 255),
                    min(max(a[2] + (rate[2] * (line - y1)), 0), 255)
                )
                fn_line(surface, color, (x1, line), (x2, line))
        else:
            for col in range(x1, x2):
                color = (
                    min(max(a[0] + (rate[0] * (col - x1)), 0), 255),
                    min(max(a[1] + (rate[1] * (col - x1)), 0), 255),
                    min(max(a[2] + (rate[2] * (col - x1)), 0), 255)
                )
                fn_line(surface, color, (col, y1), (col, y2))


    fill_gradient(screen, YELLOW, VIOLET)

    # --- Drawing code should go here

    pygame.draw.rect(screen, GREEN, [0, 720, 1280, 200], 200)
    pygame.draw.ellipse(screen, YELLOW, [1170, 86, 100, 100], 0)
    # Barn Entrance Curve
    pygame.draw.ellipse(screen, BLACK, [610, 574, 40, 20], 0)
    # Barn Entrance Main
    pygame.draw.rect(screen, BLACK, [610, 580, 40, 55], 0)
    # Barn Roof front
    pygame.draw.polygon(screen, FRONT_BARN_ROOF, [[590, 510], [630, 475], [670, 510]], 0)
    # Barn Front
    pygame.draw.rect(screen, DEEP_RED, [590, 510, 80, 125], 0)
    # Barn side
    pygame.draw.rect(screen, RED, [400, 510, 190, 125], 0)

    # Barn Entrance Curve
    pygame.draw.ellipse(screen, BLACK, [610, 574, 40, 20], 0)
    # Barn Entrance Main
    pygame.draw.rect(screen, BLACK, [610, 580, 40, 55], 0)
    # side of barn roof
    pygame.draw.polygon(screen, BARN_ROOF, [[435, 475], [630, 475], [590, 510], [400, 510], 0])

    # wheat loop
    x_offset = 0
    y_offset = 650
    while y_offset < 720:
        while x_offset < 1280:
            pygame.draw.line(screen, WHEAT, [x_offset - 2, y_offset], [x_offset, y_offset - 10], 1)
            x_offset = x_offset + 5
        x_offset = 0
        y_offset += 12
    pygame.draw.rect(screen, RED, [400, 510, 190, 125], 0)

    # Barn Struts loop
    barn_x_offset = 20
    for barn_x_offset in range(20, 30):
        pygame.draw.rect(screen, BROWN, [20 * barn_x_offset, 510, 10, 125], 0)
    font = pygame.font.SysFont('Tahoma', 25, True, False)
    text = font.render("JAMES OXMAN", True, BLACK)
    screen.blit(text, [20, 20])

    # --- Go ahead and update the screen with what we've drawn.
    pygame.display.flip()

    # --- Limit to 60 frames per second
    clock.tick(60)

# Close the window and quit.
pygame.quit()
